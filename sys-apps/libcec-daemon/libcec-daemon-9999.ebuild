# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-2
inherit autotools

DESCRIPTION="A simple daemon to connect libcec to uinput. That is, using your TV to control your PC! by Andrew Brampton"
HOMEPAGE="https://github.com/KingBonecrusher/libcec-daemon"
EGIT_REPO_URI="https://github.com/benklop/libcec-daemon.git"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="dev-libs/boost dev-libs/log4cplus dev-libs/libcec"
RDEPEND="${DEPEND}"

src_prepare() {
   eautoreconf
}

src_configure() {
       econf
}

src_compile() {
       emake
}


src_install()
{
	insinto /usr/bin
	dobin libcec-daemon || die "Install failed!"
}
