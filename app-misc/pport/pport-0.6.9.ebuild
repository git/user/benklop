# Copyright 1999-2004 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: $ 

DESCRIPTION="A simple yet handy program for accessing the output pins of the parallel port."
#HOMEPAGE="${PN}.sf.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86 amd64"
DEPEND="sys-libs/glibc
	ssl? ( dev-libs/openssl )"
IUSE="ssl"

src_compile() {
	local myconf
	use ssl && myconf='--enable-ssl=yes'
	econf ${myconf} || die
	emake || die
}

src_install() {
	dobin src/pport src/pportd
	dodoc AUTHORS ChangeLog COPYING INSTALL NEWS THANKS  doc/*
	doman man/pport.1 man/pportd.1
}
