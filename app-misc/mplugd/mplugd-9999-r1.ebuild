# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-2 distutils user

DESCRIPTION="Daemon that executes event-triggered actions"
HOMEPAGE="http://github.com/anyc/mplugd"
EGIT_REPO_URI="https://github.com/benklop/mplugd.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="pulseaudio X udev midi"

RDEPEND="
	pulseaudio? ( dev-python/dbus-python )
	X? ( >dev-python/python-xlib-0.15 )
	udev? ( dev-python/pyudev )
	midi? ( media-libs/portmidi )
	pulseaudio? ( <dev-python/pygobject-3 )"

pkg_preinst() {
    enewgroup mplugd
	enewuser mplugd -1 -1 -1 "mplugd,pulse-access,daemon,lirc"
}


