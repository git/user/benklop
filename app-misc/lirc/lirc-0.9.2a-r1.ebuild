# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit systemd user
DESCRIPTION="decode and send infra-red signals of many commonly used remote controls"
HOMEPAGE="http://www.lirc.org/"

MY_P=${PN}-${PV/_/}

if [[ "${PV/_pre/}" = "${PV}" ]]; then
    SRC_URI="mirror://sourceforge/lirc/${MY_P}.tar.bz2"
else
    SRC_URI="http://www.lirc.org/software/snapshots/${MY_P}.tar.bz2"
fi

LICENSE="GPL-2
         MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="x11-libs/libX11
        x11-libs/libSM
		x11-libs/libICE
		"
RDEPEND="${DEPEND}"
src_prepare() {
	epatch "${FILESDIR}/lircd.service.patch"
}

pkg_preinst() {
    enewgroup lirc
    enewuser lirc -1 -1 -1 "lirc,daemon"
}
