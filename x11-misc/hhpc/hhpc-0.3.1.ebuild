# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"
inherit git-r3

DESCRIPTION="hhpc is a lightweight alternative to unclutter. it is essentially hpc written in C."
HOMEPAGE="https://github.com/aktau/hhpc/"
SRC_URI=""

EGIT_REPO_URI="https://github.com/aktau/hhpc"
EGIT_BRANCH="master"
EGIT_TAG="v0.3.1"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="x11-libs/libX11"
RDEPEND="${DEPEND}"

src_compile() {
    emake
}

src_install()
{
    insinto /usr/bin
    dobin hhpc || die "Install failed!"
}

